<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use View;
use Carbon\Carbon;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $age =Carbon::createFromDate(1995,7,3)->age;
        View::share('age', $age);
        View::share('myname', 'Pallab');
        //View::share('auth', Auth::user());
        View::composer('*',function($view){
            $view->with('auth',Auth::user());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
