<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
class PagesController extends Controller
{
    public  function index(){
        if(View::exists('pages.index'))
            return view('pages.index')
                ->with('text','<b>Laravel</b>')
                ->with('name','Pallab')
                ->with(['location'=>'Bangladesh','planate'=>'Earth']);
       // return view('pages.index',['text'=>'<b>Laravel</b>']);
        else
            return 'No view available';
    }
    public function profile(){
        return view('pages.profile');
    }
    public function settings(){
        return view('pages.settings');
    }
    public  function blade(){
        $gender  ='fefamale';
        $text= 'Hello There!';
        return view('blade.bladetest', compact('gender','text'));
    }
}
