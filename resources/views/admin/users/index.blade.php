@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="com-md-6 col-md-offset-3">

            <h3>{{$users->total()}} Total users</h3>
            <b>In this page({{$users->count()}} users)</b>
            @if(count($users) > 0)

            <ul class="list-group">
                @foreach($users as $user)
                    <li class="list-group-item" style=" margin-top:20px">
                       <span>
                           {{$user->name}}
                       </span>
                        <span class="pull-right ">
                            Joined ({{$user->created_at->diffForHumans()}}})
                            <button class="btn btn-xs btn-primary ">Fellow</button>
                        </span>
                    </li>
                @endforeach
                {{$users->links()}}
            </ul>
            @else
                <p>No users Available</p>
            @endif
        </div>
    </div>
@endsection



